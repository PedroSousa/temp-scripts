<?php
$it = new FilesystemIterator(dirname(__FILE__).'/input');

foreach ($it as $fileinfo) {

    if($fileinfo->isDir()) {

        mkdir((dirname(__FILE__).'/output/'.$fileinfo->getFilename()), 0777, true);
        $folders = new FilesystemIterator(dirname(__FILE__).'/input/'.$fileinfo->getFilename());

        foreach($folders as $fileinfo2) {
           // echo $fileinfo2->getFilename() . "\n";
            $currentFolder = $fileinfo2->getFilename();
            $position = 1;

            if($fileinfo2->isDir()) {
                mkdir((dirname(__FILE__).'/output/'.$fileinfo->getFilename().'/'.$fileinfo2->getFilename()), 0777, true);
                $files = new FilesystemIterator(dirname(__FILE__).'/input/'.$fileinfo->getFilename().'/'.$fileinfo2->getFilename());
        
                foreach($files as $file) {

                    if($file->getFilename() == '.DS_Store') {
                        continue;
                    }

                    echo $file->getFilename() . "\n";
                    rename(
                        dirname(__FILE__).'/input/'.$fileinfo->getFilename().'/'.$fileinfo2->getFilename().'/'.$file->getFilename(),
                        dirname(__FILE__).'/output/'.$fileinfo->getFilename().'/'.$fileinfo2->getFilename().'/'.$currentFolder.'_'.$position.'.'.$file->getExtension()
                    );
                    $position++;
                }
            }
        }
    }

}